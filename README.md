# block-outside-container

Repo has moved to https://gitlab.com/ktpanda/block-outside-container

If you have cloned this repo, you can update the remote URL with this command:

```
git remote set-url origin https://gitlab.com/ktpanda/block-outside-container.git
```